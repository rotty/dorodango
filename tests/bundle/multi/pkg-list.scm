(package (multi-core (0))
  (libraries (sls -> "multi")))

(package (multi-tools (0))
  (programs (("programs" "multi.sps") -> "multi")))

;; Local Variables:
;; scheme-indent-styles: ((package 1))
;; End:
