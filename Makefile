srcdir = .

IMPL_ARGS =
ifneq ($(IMPL),)
IMPL_ARGS = -i $(IMPL)
endif

VERSION_SUFFIX ?= $(shell date +%Y%m%d)
GIT_VERSION = $(shell git describe --first-parent --match 'v*' | sed 's/^v//')

bundle:
	$(srcdir)/setup $(IMPL_ARGS) dist $(VERSION_SUFFIX)

dist:
	$(srcdir)/scripts/git-archive-all -o dorodango-$(GIT_VERSION).tar.bz2 -p dorodango-$(GIT_VERSION)
