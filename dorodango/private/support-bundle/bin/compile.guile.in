#!@DORO_RUN@
!#

(import (ice-9 regex))

(define ignored-files-rx
  (make-regexp "^(srfi/.*|.*\\.(mzscheme|ikarus|ypsilon|mosh)\\.sls)$"))

(define (with-read-options opts thunk)
  (let ((saved-options (read-options)))
    (dynamic-wind
        (lambda ()
          (read-options opts))
        thunk
        (lambda ()
          (read-options saved-options)))))

(define (maybe-compile-library relative-filename absolute-filename go-file)
  (cond ((regexp-exec ignored-files-rx relative-filename)
         #f)
        (else
         (with-fluids ((current-reader #f))
           (with-read-options '(r6rs-hex-escapes
                                keywords #f
                                positions
                                square-brackets
                                hungry-eol-escapes)
             (lambda ()
               (save-module-excursion
                (lambda ()
                  (set-current-module (make-fresh-user-module))
                  (compile-file absolute-filename #:output-file go-file)
                  (load-compiled go-file))))))
         go-file)))

(define (strip-extensions path extensions)
  (cond ((null? extensions)
         path)
        ((string-suffix? (car extensions) path)
         (substring path 0 (- (string-length path)
                              (string-length (car extensions)))))
        (else
         (strip-extensions path (cdr extensions)))))

(let loop ((entry (read)))
  (cond ((not (eof-object? entry))
         (let* ((library-dir (car entry))
                (library-path (cadr entry))
                (go-path (string-append
                          (strip-extensions library-path
                                            '(".guile.sls" ".sls"))
                          ".go"))
                (compiled-dir (caddr entry)))
           (let ((result (maybe-compile-library
                          library-path
                          (string-append library-dir library-path)
                          (string-append compiled-dir "guile/" go-path))))
             (write result)
             (newline)
             (force-output)
             (loop (read)))))))

;; Local Variables:
;; mode: scheme
;; End:
