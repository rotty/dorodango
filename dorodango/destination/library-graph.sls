#!r6rs
;;; library-graph.sls --- Graph of library dependencies

;; Copyright (C) 2011, 2012 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:


(library (dorodango destination library-graph)
  (export destination-library-graph
          library-graph-topological-fold
          
          library-node?
          library-node-name
          library-node-dependencies
          library-node-pathname)
  (import (rnrs)
          (only (srfi :1) span)
          (only (srfi :13)
                string-index-right
                string-suffix?)
          (srfi :67 compare-procedures)
          (wak wt-tree)
          (wak foof-loop)
          (wak foof-loop nested)
          (spells match)
          (spells pathname)
          (spells record-types)
          (dorodango inventory)
          (dorodango destination)
          (dorodango package))

(define (library-name-compare x y)
  (list-compare symbol-compare x y))

(define library-name-wt-type
  (make-wt-tree-type (>? library-name-compare)))

(define library-name=? (=? library-name-compare))

(define (library-pathnames inventory implementation)
  (loop continue ((for cursor path (in-inventory-leafs inventory))
                  (with pathnames '()))
    => pathnames
    (cond ((matching-library-name (inventory-name cursor) implementation)
           => (lambda (name)
                (let* ((path (reverse path))
                       (cursor-name (inventory-name cursor)))
                  (continue
                   (=> pathnames
                       (cons (cons (append (map string->symbol path) (list name))
                                   (make-pathname #f path cursor-name))
                             pathnames))))))
          (else
           (continue)))))

(define-record-type* library-node
  (make-library-node name pathname dependencies)
  ())

(define-functional-fields library-node name pathname dependencies)

(define (destination-library-graph destination package implementation)
  (loop continue
        ((for name.pathname
              (in-list (library-pathnames
                        (package-category-inventory package 'libraries)
                        implementation)))
         (with graph (make-wt-tree library-name-wt-type)))
    => graph
    (let ((relative-pathname (cdr name.pathname)))
      (cond ((snarf-library (destination-pathname destination
                                                  package
                                                  'libraries
                                                  relative-pathname))
             => (lambda (name.dependencies)
                  (let ((name (car name.dependencies)))
                    (cond ((library-name=? name (car name.pathname))
                           (let ((info (make-library-node
                                        name
                                        relative-pathname
                                        (cdr name.dependencies))))
                             (continue
                              (=> graph (wt-tree/add graph name info)))))
                          (else
                           (continue))))))
            (else
             (continue))))))

(define (trim-library-graph graph)
  (wt-tree/fold
   (lambda (key info trimmed)
     (let ((trimmed-info
            (library-node-modify-dependencies
             info
             (lambda (dependencies)
               (filter (lambda (dependency)
                         (wt-tree/lookup graph dependency #f))
                       dependencies)))))
       (wt-tree/add trimmed key trimmed-info)))
   (make-wt-tree library-name-wt-type)
   graph))

(define (library-graph-topological-fold graph kons knil)
  (wt-tree/topological-fold graph library-node-dependencies kons knil))

(define (snarf-library pathname)
  (call-with-port (open-file-input-port (->namestring pathname)
                                        (file-options)
                                        (buffer-mode block)
                                        (make-transcoder (utf-8-codec)))
    (lambda (port)
      (match (read port)
        (('library (name ___)
           ('export . exports)
           ('import . import-forms)
           . body)
         (cons (strip-library-version name)
               (extract-imported-libraries import-forms)))
        (_
         #f)))))

(define (strip-library-version reference)
  (span symbol? reference))

(define (extract-imported-libraries import-specs)
  (loop recur ((with names '())
               (for import-spec (in-list import-specs)))
    => names
    (match import-spec
      (((or 'only 'except 'prefix 'rename 'for) set . details)
       (recur (=> names (append (extract-imported-libraries (list set))
                                names))))
      (('library reference)
       (recur (=> names (cons (strip-library-version reference)
                              names))))
      (reference
       (recur (=> names (cons (strip-library-version reference) names)))))))

(define (matching-library-name filename implementation)
  (and (string-suffix? ".sls" filename)
       (let ((end (- (string-length filename) 4)))
         (cond ((string-index-right filename #\. 0 end)
                => (lambda (dot-index)
                     (and (string=? (substring filename (+ dot-index 1) end)
                                    (symbol->string implementation))
                          (string->symbol (substring filename 0 dot-index)))))
               (else
                (string->symbol (substring filename 0 end)))))))

(define (wt-tree/topological-fold tree get-children kons knil)
  (define (visit key value state)
    (let ((discovered (car state))
          (seed (cdr state)))
      (if (wt-tree/lookup discovered key #f)
          state
          (let ((state (fold-left
                        (lambda (state child)
                          (cond ((wt-tree/lookup tree child #f)
                                 => (lambda (value)
                                      (visit child value state)))
                                (else
                                 state)))
                        (cons (wt-tree/add discovered key #t) seed)
                        (get-children value))))
            (cons (car state) (kons key value (cdr state)))))))
  (cdr (wt-tree/fold
        (lambda (key value state)
          (visit key value state))
        (cons (make-wt-tree (wt-tree/type tree)) knil)
        tree)))

)
