#!r6rs
;;; compilation-hook.sls --- Destination hook for compiling libraries

;; Copyright (C) 2012 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(library (dorodango destination compilation)
  (export compilation-hook
          logger:dorodango.compilation)
  (import (except (rnrs) delete-file file-exists?)
          (srfi :8 receive)
          (wak foof-loop)
          (wak foof-loop nested)
          (spells alist)
          (spells pathname)
          (spells filesys)
          (spells process)
          (spells logging)
          (dorodango private utils)
          (dorodango inventory)
          (dorodango destination)
          (dorodango destination library-graph))

(define (compilation-hook destination package)
  (let* ((info (destination-info destination))
         (pathname (destination-support-pathname destination
                                                 'package-executables
                                                 '(("bin") "compile"))))
    (if (and (file-exists? pathname)
             (file-executable? pathname))
        (list (run-compile-process pathname
                                   destination
                                   package
                                   (assq-ref info 'implementation)))
        '())))

(define (run-compile-process pathname destination package implementation)
  (let* ((process (spawn-process #f #f #f (standard-error-port) pathname))
         (in-port (transcoded-port (process-input process)
                                   (make-transcoder (utf-8-codec))))
         (out-port (transcoded-port (process-output process)
                                    (make-transcoder (utf-8-codec))))
         (libraries-directory (destination-pathname destination
                                                    package
                                                    'libraries
                                                    '(())))
         (compiled-directory (destination-support-pathname
                              destination
                              'compiled-libraries
                              '(()))))
    (iterate-values ((compiled-libraries
                      (make-inventory 'compiled-libraries #f)))
        => (begin
             (close-port in-port)
             (close-port out-port)
             (receive (status signal) (wait-for-process process)
               (unless (eqv? status 0)
                 (error 'compile-libraries
                        "compilation returned failure status: " status)))
             compiled-libraries)
        (for relative-pathname (in-list (libraries-to-compile destination
                                                              package
                                                              implementation)))
      (let ()
        (put-datum in-port (map ->namestring (list libraries-directory
                                                   relative-pathname
                                                   compiled-directory)))
        (log/compilation 'info "compiling "
                         (dsp-pathname (merge-pathnames relative-pathname
                                                        libraries-directory)))
        (newline in-port)
        (flush-output-port in-port)
        (let ((filename (get-datum out-port)))
          (cond (filename
                 (log/compilation 'info "compiled " filename)
                 (inventory-add-pathname compiled-libraries (->pathname filename)))
                ((not filename)
                 (log/compilation 'info "ignored for compilation")
                 compiled-libraries)
                (else
                 ;;++ EOF handling
                 compiled-libraries
                 )))))))

(define (inventory-add-pathname inventory pathname)
  (if (pathname-file pathname)
      (let ((depth (+ 1 (length (pathname-directory pathname)))))
        (inventory-leave-n
         (inventory-update inventory
                           (append (pathname-directory pathname)
                                   (list (file-namestring pathname)))
                           #f
                           #f)
         depth))
      inventory))

(define (libraries-to-compile destination package implementation)
  (reverse
   (library-graph-topological-fold
    (destination-library-graph destination package implementation)
    (lambda (name info list)
      (cons (library-node-pathname info) list))
    '())))

(define (destination-info destination)
  (collect-list (for entry
                     (in-file (->namestring
                               (destination-support-pathname destination
                                                             'package-auxiliaries
                                                             "info"))
                              read))
    entry))

(define logger:dorodango.compilation
  (make-logger logger:dorodango 'compilation))
(define log/compilation (make-fmt-log logger:dorodango.compilation))

)
