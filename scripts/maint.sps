#!r6rs
;;; maint.sps --- Maintainance tool for dorodango

;; Copyright (C) 2009-2011, 2015 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(import (except (rnrs) delete-file file-exists?)
        (srfi :19 time)
        (srfi :45 lazy)
        (wak foof-loop)
        (wak foof-loop nested)
        (wak fmt)
        (spells match)
        (spells pathname)
        (spells filesys)
        (spells process)
        (spells sysutils)
        (only (dorodango private utils) rm-rf)
        (dorodango package)
        (dorodango bundle)
        (dorodango actions)
        (dorodango ui formatters))

(define bundle-base (make-pathname #f '(packages) #f))

(define transitive-dependencies
  '(srfi
    wak-common
    wak-syn-param
    wak-riastreams
    wak-foof-loop
    wak-fmt
    wak-irregex
    wak-parscheme
    wak-trc-testing
    wak-wt-tree
    wak-ssax
    spells
    industria
    ocelotl))

;; Returns a list of (<name> <dir-pathname> <package> ...) entries
(define (included-packages bundle-base package-names)
  (collect-list (for name (in-list package-names))
      (package-list-entry name (pathname-with-file bundle-base name))))

(define (package-list-entry name directory)
  (let ((directory (pathname-as-directory directory)))
    (call-with-input-bundle directory (bundle-options no-inventory)
      (lambda (bundle)
        (cons* name directory (bundle-packages bundle))))))

(define (dsp-package-entry entry)
  (match entry
    ((name directory . packages)
     (cat "  - " name
          ": " (fmt-join dsp-package-identifier packages ", ")))))

(define (create-readme pathname directory.packages-list)
  (let ((main-package (caddar directory.packages-list)))
    (call-with-output-file/atomic pathname
      (lambda (port)
        (let ((title (fmt #f "Dorodango "
                          (dsp-package-version
                           (package-version main-package)))))
          (fmt port
               title nl
               (make-string (string-length title) #\-) nl
               nl
               (wrap-lines
                "This is a full release archive of Dorodango, "
                "including all dependencies.  For setup instructions, see "
                "the Dorodango manual, available online at "
                "<http://home.gna.org/dorodango/manual/>, or as Texinfo source "
                "in the directory ./dorodango/docs/.")
               nl
               (wrap-lines
                "The following packages have been bundled in this archive:")
               nl
               (fmt-join/suffix dsp-package-entry directory.packages-list nl)
               nl
               "--" nl
               "Andreas Rottmann, " (date->string (current-date) "~1") nl))))))

(define (run-dist name-suffix append-version)
  (let* ((dorodango-entry (package-list-entry 'dorodango
                                              (make-pathname #f '() #f)))
         (directory.packages-list
          (cons dorodango-entry
                (included-packages bundle-base transitive-dependencies)))
         (package (list-ref dorodango-entry 2))
         (renamed-package
          (make-package (string->symbol (string-append
                                         (symbol->string (package-name package))
                                         name-suffix))
                        (append (package-version package) append-version)))
         (renamed-filename (package->string renamed-package "-"))
         (tmp-dir (create-temp-directory '(())))
         (dist-dir (pathname-join tmp-dir `((,renamed-filename)))))
    (loop ((for entry (in-list directory.packages-list)))
      (match entry
        ((name directory . packages)
         (loop ((for pathname (in-list (list-files directory '(())))))
           (let ((src-pathname (pathname-join directory pathname))
                 (dest-pathname (pathname-join dist-dir
                                               `((,name))
                                               pathname)))
             ;; Exclude submodules, all located in packages/
             (unless (pathname=? (pathname-container src-pathname)
                                 (make-pathname #f '("packages") #f))
               (create-directory* (pathname-container dest-pathname))
               (create-hard-link src-pathname dest-pathname)))))))
    (create-readme (pathname-with-file dist-dir "README")
                   directory.packages-list)
    (run-process #f
                 (force %tar-path)
                 "-C" tmp-dir
                 "-cjf" (string-append renamed-filename ".tar.bz2")
                 renamed-filename)
    (rm-rf tmp-dir)))

(define %tar-path
  (delay (or (find-exec-path "tar")
             (die "`tar' not found in PATH."))))

(define (die formatter)
  (fmt (current-error-port) (cat "tool.sps: " formatter "\n"))
  (exit #f))

(define (main argv)
  (match (cdr argv)
    (("dist")
     (run-dist "-full" '()))
    (("dist" append-version)
     (run-dist "-full" (string->package-version append-version)))
    (args
     (cond ((< (length args) 2)
            (die (cat "need at least one argument.")))
           (else
            (die (cat "invalid invocation: " (fmt-join dsp args " ") ".")))))))

(main (command-line))

;; Local Variables:
;; scheme-indent-styles: (foof-loop (match 1))
;; End:
