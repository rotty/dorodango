@node Quickstart
@section Quickstart

For the unpatient, this section presents the minium you need to know
to use dorodango for installing software.

@subheading Configuration

So, you've successfully installed dorodango, and were able to get the
help message via @command{doro --help}? Then it's time to tell dorodango
where it can find software (@pxref{Packages}) to install. Create the
file @file{~/.config/dorodango/config.scm} and add this line:

@lisp
(repository experimental "http://rotty.yi.org/doro/experimental")
@end lisp

This tells dorodango the location of the author's experimental
repository, and gives it the name @samp{experimental}. You could add
further repositories with different names and locations, and dorodango
will consider them all when installing packages.

@subheading Updating the package database

This is all configuration that is needed; if you now run @command{doro
update}, you should see something like the following:

@verbatim
Fetching http://rotty.yi.org/doro/experimental/available.scm
Loading available information for repository `experimental'
@end verbatim

Now dorodango has obtained the information of available packages from
the repository. You can verify that by running @command{doro list
--all}, which should produce output resembling the following:

@verbatim
u conjure                    0-20091204
u dorodango                  0-20091204
u fidfw                      0-20091204
...
@end verbatim

The rightmost columns indicates the package state (@samp{u} means
"uninstalled"), the other columns are the package name and version.

@subheading Installing software

You can now install any of the listed packages, using @command{doro
install @var{package-name}}:

@verbatim
% doro install spells
The following NEW packages will be installed:
  spells srfi{a}
Do you want to continue? [Y/n] 
Fetching http://rotty.yi.org/doro/experimental/srfi_0-20091204.zip
Installing srfi (0-20091204) ...
Fetching http://rotty.yi.org/doro/experimental/spells_0-20091204.zip
Installing spells (0-20091204) ...
@end verbatim

As demonstrated in the above verbatim, dorodango knows that the package
@samp{spells} depends on @samp{srfi}, and will automatically install
that package as well.

@subheading Other important commands

Now you you know how to achieve the primary task of dorodango:
installing software. There are a few other things you probably want to
do at times:

@table @command
@item doro upgrade
Attempts to upgrade each package to the newest available version.

@item doro remove
Allows you to remove packages from your system.
@end table

@subheading Getting help

For each command, you can invoke @command{doro @var{command} --help}, and it will
show you what options and argument that command requires:

@verbatim
% doro remove --help
Usage: doro remove PACKAGE...
  Remove packages.

Options:
  --no-depends  ignore dependencies
  --help        show this help and exit
@end verbatim
